Установка

1. composer install
2. php artisan storage:link

В файле .env прописать актуальный адрес в APP_URL

Запрос

POST /api/upload
Поле images - принимает base64, url, multipart/form-data

Ответ в случае ошибки 
{
    "code": false,
    "response": {
        "error": "No image"
    }
}

При успешной загрузке
{
    "code": true,
    "response": [
        {
            "original": "http://uploadapi/storage/images/1548271428.jpg",
            "preview": "http://uploadapi/storage/images/1548271428_preview.jpg"
        }
    ]
}