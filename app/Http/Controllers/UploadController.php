<?php

namespace App\Http\Controllers;

use Faker\Provider\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UploadController extends Controller
{
    public function upload(Request $request)
    {
        $images = $request->get('images') ? $request->get('images') : $request->file('images');

        if (!is_array($images)) {
            $images = [$images];
        }

        $response = [
            'code' => false,
            'response' => [],
        ];

        if (!$images) {
            $response['response'] = [
                'error' => 'No image',
            ];

            return $response;
        }

        foreach ($images as $image) {
            try {
                $file = Image::make($image);
                $fileName = 'public/images/' . time() . '.jpg';
                $fileNamePreview = 'public/images/' . time() . '_preview.jpg';

                Storage::put($fileName, (string) $file->encode());
                Storage::put($fileNamePreview, (string) $file->resize(100, 100)->encode());

                $response['code'] = true;
                $response['response'][] = [
                    'original' => url(str_replace('public', 'storage', $fileName)),
                    'preview' => url(str_replace('public', 'storage', $fileNamePreview)),
                ];
            } catch (\Exception $e) {
                $response['response'][] = [
                    'error' => 'no image',
                ];
            }
        }

        return $response;
    }
}
